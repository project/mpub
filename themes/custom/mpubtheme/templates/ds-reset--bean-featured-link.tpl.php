<?php

/**
 * @file
 * Featured link bean layout
 */
?>
<div class="featured-link">
	<img src="<?php print file_create_url($field_link_image[0]['uri']); ?>" alt="" />
  <?php $field = field_view_field('bean', $variables['elements']['#entity'], 'field_link', 'default'); print drupal_render($field); ?>
</div>
