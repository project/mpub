<?php
/**
 * @file
 * Template for the responsive 2 column layout.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */

?>
<div<?php print $attributes ?>>

  <?php if (!empty($content['top'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['top'])?>>
      <?php print $content['top'] ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['featured_content'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['featured_content'])?>>
      <?php print $content['featured_content'] ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['maps_overview'])): ?>
    <div id="section-overviews">
      <ul>
        <?php if (!empty($content['maps_overview'])): ?>
          <li><i class="fa fa-map-o"></i><a href="#maps-overview" title="Maps Overview" class="maps-overview-select"><?php print t('Maps'); ?></a></li>
        <?php endif; ?>
        <?php if (!empty($content['projects_overview'])): ?>
          <li><i class="fa fa-paperclip"></i><a href="#projects-overview" title="Projects Overview" class="projects-overview-select"><?php print t('Projects'); ?></a></li>
        <?php endif; ?>
        <?php if (!empty($content['documents_overview'])): ?>
          <li><i class="fa fa-files-o"></i><a href="#documents-overview" title="Documents Overview" class="documetns-overview-select"><?php print t('Documents'); ?></a></li>
        <?php endif; ?>
      </ul>
      <?php if (!empty($content['maps_overview'])): ?>
        <div<?php print drupal_attributes($region_attributes_array['maps_overview'])?>>
          <?php print $content['maps_overview'] ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['projects_overview'])): ?>
        <div<?php print drupal_attributes($region_attributes_array['projects_overview'])?>>
          <?php print $content['projects_overview'] ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['documents_overview'])): ?>
        <div<?php print drupal_attributes($region_attributes_array['documents_overview'])?>>
          <?php print $content['documents_overview'] ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content['bottom'])): ?>
    <div<?php print drupal_attributes($region_attributes_array['bottom'])?>>
      <?php print $content['bottom'] ?>
    </div>
  <?php endif; ?>

</div>
