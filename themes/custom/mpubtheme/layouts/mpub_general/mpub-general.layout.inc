name = Mpub General
description = A simple full width layout based on omega simple
preview = preview.png
template = mpub-general-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[navigation]     = Navigation
regions[content]        = Content
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/mpub-general/mpub-general.layout.css
