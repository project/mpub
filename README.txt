Contrib / library versions and patches etc are all maintained
in mpub.make

To re-download all the contrib modules / libraries run the 
following command with drush.

drush make drupal-org.make --no-core --contrib-destination .

The Font-awesome library will need to be manually installed because of it's
licensing requirements.
Download and extract the contents of the Font Awesome archive file to the
DRUPAL_ROOT/profiles/mpub/libraries/fontawesome folder so that the following
file exists:
DRUPAL_ROOT/profiles/mpub/libraries/fontawesome/fonts/fontawesome-webfont.ttf