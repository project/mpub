api = 2
core = 7.x
defaults[projects][subdir] = contrib

; Modules
projects[admin_menu][version] = "3.0-rc5"
projects[autocomplete_deluxe][version] = "2.2"
projects[better_exposed_filters][version] = "3.4"
projects[ctools][version] = "1.12"
projects[date][version] = "2.8"
projects[diff][version] = "3.2"
projects[ds][version] = "2.16"
projects[email][version] = "1.3"
projects[entity][version] = "1.9"
projects[entityreference][version] = "1.5"
projects[features][version] = "2.10"
projects[field_collection][version] = "1.0-beta8"
projects[field_group][version] = "1.5"
projects[fontawesome][version] = "2.1"
projects[geofield][version] = "2.3"
projects[geofield][patch][] = "https://www.drupal.org/files/issues/openlayers-widget-show-title-description-2045325-5.patch"
projects[geofield][patch][] = "https://www.drupal.org/files/issues/multiple-draw-widgets-patch-2041169-6.patch"
projects[geophp][version] = "1.7"
projects[panels][version] = "3.9"
projects[imce][version] = "1.9"
projects[imce_wysiwyg][version] = "1.0"
projects[kml][version] = "1.0-alpha1"
projects[libraries][version] = "2.2"
projects[link][version] = "1.3"
projects[menu_admin_per_menu][version] = "1.0"
projects[nice_menus][version] = "2.5"
projects[openlayers][version] = "2.0-beta11"
projects[openlayers][patch][] = "https://www.drupal.org/files/issues/osm_for_default-2764999-5.patch"
projects[pathauto][version] = "1.2"
projects[proj4js][version] = "1.2"
projects[quicktabs][version] = "3.6"
projects[responsive_menus][version] = "1.5"
projects[strongarm][version] = "2.0"
projects[taxonomy_access_fix][version] = "2.1"
projects[token][version] = "1.5"
projects[views][version] = "3.17"
projects[views_data_export][version] = "3.2"
projects[views_responsive_grid][version] = "1.3"
projects[wysiwyg][version] = "2.2+54-dev"
projects[wysiwyg_filter][version] = "1.6-rc2"
projects[bean][version] = "1.9"
projects[uuid][version] = "1.2"
projects[views_bulk_operations][version] = "3.3"
projects[node_export][version] = "3.0"
projects[node_export][patch][] = "https://www.drupal.org/files/issues/node_export-field_collection-1670740-39.patch"
projects[node_export][patch][] = "https://www.drupal.org/files/issues/views_bulk_operations_node_export_fails_with_undefined_function-1869918-33.patch"

projects[multiselect][version] = "1.11"
projects[multiselect][patch][] = "https://www.drupal.org/files/issues/multiselect.fix_.notices.patch"


projects[wms][version] = "2.x-dev"
projects[wms][download][type] = "git"
projects[wms][download][url] = "http://git.drupal.org/project/wms.git"
projects[wms][download][revision] = "e3e4116"
projects[wms][download][branch] = "7.x-2.x"

projects[wms][patch][] = "https://www.drupal.org/files/issues/wms.support-for-different-servers.7-x.patch"
projects[wms][patch][] = "https://www.drupal.org/files/issues/wms-allow_alteration_of_response-2507617-3-7.x.patch"
projects[wms][patch][] = "https://www.drupal.org/files/issues/wms-layer_names_without_namespaces-259685-1-7.x.patch"

; dev version required to fix errors from updated ctools https://www.drupal.org/node/1506268
projects[proxy][version] = "1.x-dev"
projects[proxy][download][type] = "git"
projects[proxy][download][url] = "http://git.drupal.org/project/proxy.git"
projects[proxy][download][revision] = "75520c56afbe4c6bf539fd2c4580f105b0fe03a3"
projects[proxy][download][branch] = "7.x-1.x"

; Themes
projects[omega][version] = "4.3"

;Dev version of wysiwyg for CKEditor 4.x
projects[wysiwyg][version] = "2.x-dev"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][url] = "http://git.drupal.org/project/wysiwyg.git"
projects[wysiwyg][download][revision] = "898d022cf7d0b6c6a6e7d813199d561b4ad39f8b"
projects[wysiwyg][download][branch] = "7.x-2.x"

; Dev version of SHS because it hasn't had a release since 2013
projects[shs][version] = "1.x-dev"
projects[shs][download][type] = "git"
projects[shs][download][url] = "http://git.drupal.org/project/shs.git"
projects[shs][download][revision] = "a6437be19f42e8f4ebc94f6a92a564ea1df3e47d"
projects[shs][download][branch] = "7.x-1.x"

; Dev version of hierarchical_term_formatter because of
; https://www.drupal.org/node/2469433
projects[hierarchical_term_formatter][version] = "1.x-dev"
projects[hierarchical_term_formatter][download][type] = "git"
projects[hierarchical_term_formatter][download][url] = "http://git.drupal.org/project/hierarchical_term_formatter.git"
projects[hierarchical_term_formatter][download][revision] = "e5dac7ff0dcca0c90797930584d673fb6bb711e2"
projects[hierarchical_term_formatter][download][branch] = "master"


;Libraries
; libraries[openlayers][download][type] = "file"
; libraries[openlayers][download][url] = http://github.com/openlayers/openlayers/releases/download/release-2.13.1/OpenLayers-2.13.1.zip
; libraries[openlayers][directory_name] = openlayers
; libraries[openlayers][destination] = "libraries"

; libraries[ckeditor][download][type] = "file"
; libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.1/ckeditor_4.4.1_standard.zip
; libraries[ckeditor][directory_name] = ckeditor
; libraries[ckeditor][destination] = "libraries"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
;libraries[proj4js][download][type] = ""
;libraries[proj4js][download][url] = ""
;libraries[proj4js][directory_name] = "proj4js"
;libraries[proj4js][type] = "library"


; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
;libraries[fontawesome][download][type] = ""
;libraries[fontawesome][download][url] = ""
;libraries[fontawesome][directory_name] = "fontawesome"
;libraries[fontawesome][type] = "library"
