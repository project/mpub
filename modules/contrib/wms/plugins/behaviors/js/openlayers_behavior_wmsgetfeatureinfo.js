// $Id$

/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * WMSGetFeatureinfo Behavior
 * http://dev.openlayers.org/releases/OpenLayers-2.9/doc/apidocs/files/OpenLayers/Control/WMSGetFeatureInfo-js.html
 */

(function ($) {

  //Initialize settings array.  
  Drupal.openlayers.openlayers_behavior_wmsgetfeatureinfo = 
  Drupal.openlayers.openlayers_behavior_wmsgetfeatureinfo || {

    //initialized when behaviour is added
    htmlelement : {},
    
    wmslayers: {},
    
    //get the appropriate wrapper element for a wms feature info request
    getWrapper : function(url) {
      
      if ($('a[href$="'+url+'"]', this.htmlelement).length == 0) {
        var wrapper = $('<div><a href="'+url+'"></a><div class="wms-feature-content"></div></div>'); 
        this.htmlelement.append(wrapper);          
        return wrapper;
      }
      else {
        return $('a[href="'+url+'"]', this.htmlelement).parent('div');
      }    
    },

    beforegetfeatureinfo: function(url, layernames) {
      
      var this_ = this;
      
      if((typeof $.colorbox == 'function') && ($("#popup").length == 0)) {
        $.colorbox({
          Title: "Results.",
          height: "80%",
          width: "80%",
          opacity: ".25",
          inline: true,
          href:"#" + this_.htmlelement
        });
      } else {
        this_.htmlelement.parent().css("display", "block");
      }
      
      var status_el = $('div.wms-feature-content', this_.getWrapper(url));
  
      if (this_.getfeatureinfo_usevisiblelayers == false) { 
        status_el.html(Drupal.t('Searching...'));
        return; 
      }
      
      var layer_titles = [];
      $.each(this_.wmslayers[url], function(i, wmslayer){
        layer_titles.push(wmslayer.name);
      });

            
      if (layernames.length == 0 ) {
        status_el.html(Drupal.t('No layer selected'));
        return;
      } else {
        status_el.html(Drupal.t('Searching in ' + layer_titles.join(", ")));
      }
      return;
    },
  
    fillHTML : function(result, url) {
      var this_ = this;
      if (this_.getfeatureinfo_highlight 
        && this_.getfeatureinfo_info_format == 'application/vnd.ogc.gml') {
        var reader = new OpenLayers.Format.GML();
        var features = reader.read(result);
        var layer = this_.hlLayer[0];
        layer.removeAllFeatures();
        for (var i = 0; i < features.length; i++) {
          this_.hlLayer[0].addFeatures([features[i]]);
          if (Drupal.openlayers.popup.popupSelect) {
            Drupal.openlayers.popup.popupSelect.select(features[i]);
          }
        }
      }
      else
      {        
        $('div.wms-feature-content', this_.getWrapper(url)).html(result);
        Drupal.attachBehaviors(this_.htmlelement);
      }
    },
  
    fillHTMLerror : function(result) {
      this_.htmlelement.html(result);
      Drupal.attachBehaviors(this_.htmlelement);
    },    
  };
  
  //Add the wmsgetfeatureinfo behavior
  Drupal.openlayers.addBehavior('openlayers_behavior_wmsgetfeatureinfo', function (data, options) {
    var map = data.openlayers;
    var layer;
    var layers = [];

    if (data.map.behaviors['openlayers_behavior_wmsgetfeatureinfo']) {
      if (options.getfeatureinfo_usevisiblelayers == false) { 
        layers = data.openlayers.getLayersBy('drupalID', 
          options.getfeatureinfo_layers);  // TODO Make this multiple select!           
      } 
      else {        
        $.each(data.openlayers.layers, function(i, layer) {        
          if (layer.CLASS_NAME == "OpenLayers.Layer.WMS" && !layer.isBaseLayer) {
            layers.push(layer);
          }          
        });
      }

      $.extend(Drupal.openlayers.openlayers_behavior_wmsgetfeatureinfo, options, {
        hlLayer: map.getLayersBy('drupalID', "wms_highlight_layer"),
        htmlelement: $("#" + options.getfeatureinfo_htmlelement),
        layers: layers,
        map: data.openlayers,
      });
      
      OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, 
        {
          defaultHandlerOptions: {
            'single': true,
            'double': false,
            'pixelTolerance': 0,
            'stopSingle': false,
            'stopDouble': false
          },

          initialize: function(options) {
            this.handlerOptions = OpenLayers.Util.extend(
              {}, this.defaultHandlerOptions
            );
            OpenLayers.Control.prototype.initialize.apply(
              this, arguments
            ); 
            this.handler = new OpenLayers.Handler.Click(
              this, {
                'click': this.onClick,
              }, this.handlerOptions
            );
          }, 

          onClick: function(evt) {
            
            var handler = Drupal.openlayers.openlayers_behavior_wmsgetfeatureinfo;            
            var wmslayers = handler.wmslayers = {};
            
            $.each(handler.layers, function(i, layer){
            
              if (layer.visibility  && (layer.CLASS_NAME == "OpenLayers.Layer.WMS") &&
                  layer.isBaseLayer == false ) {
                
                
                var layerCopy = $.extend(true, {}, layer);
                layerCopy.params.LAYERS = [];
                layerCopy.origParams = $.extend({},  layer.params);
                
                var url = layerCopy.getFullRequestString(layerCopy.params);
                wmslayers[url] = wmslayers[url] || [];                
                wmslayers[url].push(layerCopy);             
              }
              
            });

            if ($.isEmptyObject(wmslayers)) { 
              alert ("No Layer to Query is visible.");
              return;
            }
            
            $.each(wmslayers, function(url,layers){
              var params = [];
              var layernames = [];            
              $.each(layers, function(i, layer) {
                layernames.push(layer.origParams.LAYERS.join(","));
                params = layer.params;
              });
              layernames = layernames.join(",");
              
              handler.beforegetfeatureinfo(url, layernames, wmslayers);
              
              var params = $.extend(params, {
                REQUEST: "GetFeatureInfo",
                EXCEPTIONS: "application/vnd.ogc.se_xml",
                BBOX: layers[0].getExtent().toBBOX(),
                X: Math.round(evt.xy.x),
                Y: Math.round(evt.xy.y),
                INFO_FORMAT: handler.getfeatureinfo_info_format, 
                QUERY_LAYERS: layernames,
                FEATURE_COUNT: handler.getfeatureinfo_feature_count,
                LAYERS: layernames,
                propertyName: handler.getfeatureinfo_properties || null,
                WIDTH: data.openlayers.size.w,
                HEIGHT: data.openlayers.size.h,
                highlight: handler.getfeatureinfo_highlight
              });
              var post_url = layers[0].getFullRequestString(params);
              $.ajax({ 
                 type: 'POST', 
                 url: Drupal.settings.basePath + 'openlayers/wms/wmsgetfeatureinfo',
                 data: { 
                   ajax : true, 
                   url : post_url 
                 },
                 success: function(result) {
                  handler.fillHTML(result, url)
                 }                 
                 ,
                 fail: handler.fillHTMLerror
              });
               
            });            
          }
        }
      );
      GetFeatureControl = new OpenLayers.Control.Click(); 

      data.openlayers.addControl(GetFeatureControl);
      GetFeatureControl.activate();
    
      // This is to update the OpenLayers Plus block switcher feature
      // Drupal.OpenLayersPlusBlockswitcher.redraw();  // TODO Fix this for D7
    }
  });

})(jQuery);
