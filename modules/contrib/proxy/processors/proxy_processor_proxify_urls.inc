<?php

/**
 * @file
 * Proxy Processor Plugin: proxy_processor_proxify_urls
 */

/**
 * Implementation of Proxy Processor Plugin
 */
class proxy_processor_proxify_urls {
  var $options;

  /**
   * Initiate any options
   *
   * Define any default options that can be stored
   * in the options property of the class.
   *
   * @return
   *   Array of options
   */
  function options_init() {
    return array();
  }

  /**
   * Options form
   *
   * Defines any form items needed for
   * options of the processor.
   *
   * @return
   *   Valid Drupal form array.
   */
  function options_form() {
    return array();
  }

  /**
   * Create base URL from request (URL without the path)
   *
   * @param $request_uri
   *   Requested URI
   * @return
   *   Base URL in form like http://drupal.org
   */
  function get_base_request($request_uri = '') {
    // Determine base host
    $url_parts = parse_url($request_uri);
    // Put back topgether
    return (isset($url_parts['scheme']) ? $url_parts['scheme'] . '://' : '') .
      (isset($url_parts['user']) ? $url_parts['user'] . ':' : '') .
      (isset($url_parts['pass']) ? $url_parts['pass'] . '@' : '') .
      (isset($url_parts['host']) ? $url_parts['host'] : '') .
      (isset($url_parts['port']) ? ':' . $url_parts['port'] : '');
  }

  /**
   * Main Rendering Function
   *
   * This processing basically looks in the content for matches
   * of specific attributes with variable delimters (", '), then replaces
   * the URL's appropriately.
   *
   * NOTE: These operations should be done with the DOM object in PHP,
   * but it requires a specific extesion php5-dom
   *
   * @param $response
   *   Response object to process.  Main content
   *   is in $response->data
   * @param $params
   *   Array of parameters, as sent to proxy().
   */
  function render(&$response, $params) {
    $proxy_base = base_path() . PROXY_ROUTER_PATH . '?' . PROXY_ROUTER_QUERY_VAR . '=';
    $content = $response->data;

    // If site uses 'base' then our relative links must be relative to that instead
    // eg <base href="http://cdn.example.com/">
    // Need to inspect the source first for this one.
    if (preg_match('@<base\s[^>]*href=["\']([^"\']*)["\'][^>]*>@', $content, $found)) {
      // If <base> is on a page, it totally overrides any other relative rules.
      $params['request_uri'] = $found[1];
    }

    // Siteroot of the source (no trailing slash)
    $request_base = $this->get_base_request($params['request_uri']);
    $redirect_base = $proxy_base . $request_base;
    // Effective remote dir of the source (for relative links)
    $base_parts = parse_url($params['request_uri']);
    $base_parts['path'] = (!empty($base_parts['path']) && $base_parts['path'] != '/') ? dirname($base_parts['path']) . '/' : '/';
    // Discard queries and fragments for base URLs
    unset($base_parts['query']);
    unset($base_parts['fragment']);
    $request_dir = $this->glue_url($base_parts);
    $redirect_dir = $proxy_base . $request_dir;

    // Set up arrays for matching
    $matches = array();
    // The path may or may not be quoted with single or double quotes.
    // Maintain the quoting if any.

    // Siteroot relative, (begins with /).
    $matches[] = '#(href|src|url)=(["\'])(\/[^"\']*)(["\'])#';
    $replaces[] = '$1=$2' . $redirect_base . '$3$4';
    // Relative paths (does not start with a scheme or a slash)
    $matches[] = '#(href|src|url)=(["\'])((?!\/|http|https|ftp|ftps)[^"\']*)(["\'])#';
    $replaces[] = '$1=$2' . $redirect_dir . '$3$4';
    // Full URLS (starts with a scheme)
    $matches[] = '#(href|src|url)=(["\'])((http|https|ftp|ftps)[^"\']*)(["\'])#';
    $replaces[] = '$1=$2' . $proxy_base . '$3$5';

    // CSS urls also, eg background-image.
    // background:url(/misc/tree.png) - may be unquoted. May have whitespace.

    // Siteroot relative, (begins with /).
    $matches[] = '#(url\s*)\((\s*["|\']?)(\/[^"\'\)]*)(["|\']?\s*)\)#';
    $replaces[] = '$1($2' . $redirect_base . '$3$4)';
    // Relative, no slash or scheme
    $matches[] = '#(url\s*)\((\s*["|\']?)((?!\/|http|https|ftp|ftps)[^\/"\'][^"\'\)]*)(["|\']?\s*)\)#';
    $replaces[] = '$1($2' . $redirect_dir . '$3$4)';
    // CSS using absolute URLs are doing it wrong, but they exist
    // Full URLS (starts with a scheme).
    $matches[] = '#(url\s*)\((\s*["|\']?)((http|https|ftp|ftps)[^"\'\)]*)(["|\']?\s*)\)#';
    $replaces[] = '$1($2' . $proxy_base . '$3$5)';

    // CSS using @import directives
    // May be quoted, finishes with whitespace or semicolon.
    // May have misc whitespace internally.
    // Siteroot relative,  (begins with /).
    $matches[] = '#(@import\s+)(["\']?)([\/][^"\'\;\s]*)(["\']?[\s\;])#';
    $replaces[] = '$1$2' . $redirect_base . '$3$4';
    // Relative, no slash
    $matches[] = '#(@import\s+)(["\']?)([^\/"\'\s][^"\'\;\s]*)(["\']?[\s\;])#';
    $replaces[] = '$1$2' . $redirect_dir . '$3$4';

    // Change content
    $response->data = preg_replace($matches, $replaces, $response->data);
  }

  function glue_url($parsed) {
    if (!is_array($parsed)) {
      return FALSE;
    }
    $uri = isset($parsed['scheme']) ? $parsed['scheme'] . ':' . ((strtolower($parsed['scheme']) == 'mailto') ? '' : '//') : '';
    $uri .= isset($parsed['user']) ? $parsed['user'] . (isset($parsed['pass']) ? ':' . $parsed['pass'] : '') . '@' : '';
    $uri .= isset($parsed['host']) ? $parsed['host'] : '';
    $uri .= isset($parsed['port']) ? ':' . $parsed['port'] : '';
    if (isset($parsed['path'])) {
      $uri .= (substr($parsed['path'], 0, 1) == '/') ? $parsed['path'] : ('/' . $parsed['path']);
    }
    $uri .= isset($parsed['query']) ? '?' . $parsed['query'] : '';
    $uri .= isset($parsed['fragment']) ? '#' . $parsed['fragment'] : '';
    return $uri;
  }
}
