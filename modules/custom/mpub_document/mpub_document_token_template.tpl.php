<?php
// Template for document token table
?>
<div class="field field--name-field-tokens">
    <div class="field__label">Tokens:&nbsp;</div>
    <div class="description">The below tokens can be inserted into other pages to link to this document.</div>
    <div class="field__items">
        <table class="token-hints meta">
            <thead>
            <tr>
                <th><?php print t('Token'); ?></th>
                <th><?php print t('Preview'); ?></th>
            </tr>
            </thead>
            <tbody><?php print $content ?></tbody>
        </table>
    </div>
</div>