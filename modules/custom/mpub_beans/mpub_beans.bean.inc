<?php
/**
 * @file
 * mpub_beans.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function mpub_beans_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'banner_image';
  $bean_type->label = 'Banner Image';
  $bean_type->options = '';
  $bean_type->description = 'Features an image with overlaid text and link.';
  $export['banner_image'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'featured_link';
  $bean_type->label = 'Featured Link';
  $bean_type->options = '';
  $bean_type->description = 'Feature a link as a block with an associated image.';
  $export['featured_link'] = $bean_type;

  return $export;
}
