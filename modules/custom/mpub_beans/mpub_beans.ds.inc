<?php
/**
 * @file
 * mpub_beans.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function mpub_beans_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'bean|banner_image|default';
  $ds_layout->entity_type = 'bean';
  $ds_layout->bundle = 'banner_image';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_banner_image',
        1 => 'title',
        2 => 'field_body',
        3 => 'field_link',
      ),
    ),
    'fields' => array(
      'field_banner_image' => 'ds_content',
      'title' => 'ds_content',
      'field_body' => 'ds_content',
      'field_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['bean|banner_image|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'bean|banner_image|form';
  $ds_layout->entity_type = 'bean';
  $ds_layout->bundle = 'banner_image';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'label',
        1 => 'field_banner_image',
        2 => 'title',
        3 => 'field_link',
        4 => 'field_body',
      ),
      'hidden' => array(
        5 => 'view_mode',
        6 => 'revision',
        7 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'label' => 'ds_content',
      'field_banner_image' => 'ds_content',
      'title' => 'ds_content',
      'field_link' => 'ds_content',
      'field_body' => 'ds_content',
      'view_mode' => 'hidden',
      'revision' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['bean|banner_image|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'bean|featured_link|default';
  $ds_layout->entity_type = 'bean';
  $ds_layout->bundle = 'featured_link';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_link_image',
        1 => 'title',
        2 => 'field_link',
      ),
    ),
    'fields' => array(
      'field_link_image' => 'ds_content',
      'title' => 'ds_content',
      'field_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['bean|featured_link|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'bean|featured_link|form';
  $ds_layout->entity_type = 'bean';
  $ds_layout->bundle = 'featured_link';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_reset';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'label',
        1 => 'title',
        2 => 'field_link_image',
        3 => 'field_link',
      ),
      'hidden' => array(
        4 => 'view_mode',
        5 => 'revision',
        6 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'label' => 'ds_content',
      'title' => 'ds_content',
      'field_link_image' => 'ds_content',
      'field_link' => 'ds_content',
      'view_mode' => 'hidden',
      'revision' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['bean|featured_link|form'] = $ds_layout;

  return $export;
}
