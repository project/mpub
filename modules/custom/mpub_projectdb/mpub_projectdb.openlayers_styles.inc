<?php
/**
 * @file
 * mpub_projectdb.openlayers_styles.inc
 */

/**
 * Implements hook_openlayers_styles().
 */
function mpub_projectdb_openlayers_styles() {
  $export = array();

  $openlayers_styles = new stdClass();
  $openlayers_styles->disabled = FALSE; /* Edit this to true to make a default openlayers_styles disabled initially */
  $openlayers_styles->api_version = 1;
  $openlayers_styles->name = 'clone_of_default';
  $openlayers_styles->title = 'Project style';
  $openlayers_styles->description = 'Basic default style.';
  $openlayers_styles->data = array(
    'pointRadius' => 6,
    'fillColor' => '#EE0000',
    'fillOpacity' => 0.5,
    'strokeColor' => '#EEE3DE',
    'strokeWidth' => 4,
    'strokeOpacity' => 0.69999999999999996,
    'strokeLinecap' => 'round',
    'strokeDashstyle' => 'solid',
    'graphicOpacity' => 1,
    'graphicZIndex' => 10,
    'labelAlign' => 'cm',
  );
  $export['clone_of_default'] = $openlayers_styles;

  return $export;
}
