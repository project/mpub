
(function($) {

  /**
   * Sets up interaction between field_location taxonomy dropdowns
   * and field_location_geo openlayers map + lat/lon text inputs.
   */
  Drupal.behaviors.mpub_projectdb_locations = {
    attach: function(context, settings) {

      // Watch for changes to the Region / Country dropdown, and update the
      // lat/lon text inputs and map accordingly.
      $('.field-name-field-locations', context).delegate('.shs-select', 'change', function() {
        var $select = $(this);
        var selection = $select.val();
        var coords = { lat: '', lon: ''};

        // Go back to the parent if "-None-" was selected.
        if (selection == '_none' && $select.prev('.shs-select').length) {
          $select = $select.prev('.shs-select');
          selection = $select.val();
        }

        var $fieldset = $select.parents('tr').find('.latlon-fieldset');

        // Look up the coordinates for the selected location.
        if (settings.mpub_projectdb_locations && settings.mpub_projectdb_locations[selection]) {
          coords = settings.mpub_projectdb_locations[selection];
        }

        // Update the text fields and trigger change so the map updates too.
        $fieldset.find('input:eq(0)').val(coords.lat);
        $fieldset.find('input:eq(1)').val(coords.lon).trigger('change');
        $select.blur();
      });

      // Watch for changes to the map and lat/lon text inputs, and synchronize
      // between the two.
      $('.openlayers-map', context).each(function() {
        var $map, $fieldset, data, layer, lonlat, mapProjection, formProjection;

        $map = $(this);
        $fieldset = $map.parents('tr').find('.latlon-fieldset');

        data = $map.data('openlayers');
        layer = data.openlayers.getLayersBy('drupalID', 'openlayers_behavior_geofield')[0];
        mapProjection = data.openlayers.projection;
        formProjection = new OpenLayers.Projection('EPSG:4326');

        // When the map already has a point upon loading...
        if (typeof layer.features[0] !== 'undefined') {
          // Center the map there.
          lonlat = new OpenLayers.LonLat(layer.features[0].geometry.x, layer.features[0].geometry.y);
          data.openlayers.setCenter(lonlat);

          // Store that point for the text change handler.
          $fieldset.data('map-feature', layer.features[0]);
        }

        // Event handler for when the map changes. Puts lat/lon in text inputs.
        function mapUpdateHandler(event) {
          var feature, lonlat;
          feature = event.feature;

          // We use this as a flag when triggering map updates from the form
          // so we don't write lat/lon values back to the form. The precision
          // can be different when we convert between projections.
          if (feature.bypassEvents === true) {
            feature.bypassEvents = false;
            return;
          }

          // Convert coordinates between projections.
          lonlat = new OpenLayers.LonLat(feature.geometry.x, feature.geometry.y).transform(mapProjection, formProjection);

          // Update coordinates.
          $fieldset.find('input:eq(0)').val(lonlat.lat);
          $fieldset.find('input:eq(1)').val(lonlat.lon);

          // Ensure the feature is always stored for later retrieval.
          $fieldset.data('map-feature', feature);
        }

        // Watch for map changes.
        layer.events.register('featureadded', this, mapUpdateHandler);
        layer.events.register('featuremodified', this, mapUpdateHandler);

        // Watch for text input changes.
        $fieldset.find('input').change(function() {
          var feature, $latInput, $lonInput, lat, lon, lonlat;

          feature = $fieldset.data('map-feature');

          // If the map loads with no points there will be no feature here.
          // We go ahead and create one but don't add it to the layer yet.
          if (!feature) {
            feature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(0, 0));
            $fieldset.data('map-feature', feature);
          }

          feature.bypassEvents = true;

          $latInput = $fieldset.find('input:eq(0)');
          $lonInput = $fieldset.find('input:eq(1)');

          lat = $.trim($latInput.val());
          lon = $.trim($lonInput.val());
          lonlat = new OpenLayers.LonLat(lon, lat).transform(formProjection, mapProjection);

          // Only update the map if the lat/lon is valid. Otherwise remove the
          // point from the map.
          if (!isNaN(lonlat.lon) && !isNaN(lonlat.lat)) {
            feature.geometry.x = lonlat.lon;
            feature.geometry.y = lonlat.lat;
            // If the point isn't attached to the layer yet, do that now.
            if (feature.layer == null) {
              layer.addFeatures(feature);
            }
            // When the updated point is outside the viewport, recenter the map.
            if (!feature.geometry.getBounds().intersectsBounds(data.openlayers.getExtent())) {
              data.openlayers.setCenter(lonlat);
            }
          }
          else {
            layer.removeFeatures(feature);
          }

          layer.redraw();
        });
      });

      // Openlayers maps get mangled when they initialize inside containers
      // that are hidden, like collapsible fieldsets. This is a workaround that
      // simulates a container resize to refresh the map whenever the fieldset
      // is opened. Since OpenLayers.Map.updateSize() checks before
      // and after sizes we need to actually resize something.
      $('.field-name-field-locations .fieldset-title', context).click(function() {
        var $label = $(this);
        var $map = $label.parents('tr').find('.openlayers-map').first();
        var data = $map.data('openlayers');
        var width = $map.width();

        $map.width(width + 1);
        data.openlayers.updateSize();
        $map.width(width);
      });

    }
  }

})(jQuery);
