<?php
/**
 * @file
 * mpub_projectdb.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mpub_projectdb_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_researchers|node|project|form';
  $field_group->group_name = 'group_researchers';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'project';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Researchers',
    'weight' => '6',
    'children' => array(
      0 => 'field_officer',
      1 => 'field_researcher',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Researchers',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-researchers field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_researchers|node|project|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Researchers');

  return $field_groups;
}
