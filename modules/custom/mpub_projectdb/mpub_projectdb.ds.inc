<?php
/**
 * @file
 * mpub_projectdb.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function mpub_projectdb_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|project|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'project';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'location_list' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'location_map' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|project|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function mpub_projectdb_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'location_list';
  $ds_field->label = 'Location List';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'project|*';
  $ds_field->properties = array(
    'block' => 'views|project_location_list-block_1',
    'block_render' => '1',
  );
  $export['location_list'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'location_map';
  $ds_field->label = 'Location Map';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = 'project|*';
  $ds_field->properties = array(
    'block' => 'views|project_location_map-block_1',
    'block_render' => '1',
  );
  $export['location_map'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function mpub_projectdb_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|project|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'project';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_date',
        1 => 'field_project_status',
      ),
      'left' => array(
        2 => 'body',
      ),
      'right' => array(
        3 => 'location_list',
        4 => 'location_map',
      ),
      'footer' => array(
        5 => 'field_associated_agencies',
        6 => 'field_officer',
        7 => 'field_oceanscape_goal',
        8 => 'field_notes',
        9 => 'field_existing_documents_links',
        10 => 'field_project_budget',
        11 => 'field_sdg_goal',
        12 => 'field_mdg_goal',
      ),
    ),
    'fields' => array(
      'field_date' => 'header',
      'field_project_status' => 'header',
      'body' => 'left',
      'location_list' => 'right',
      'location_map' => 'right',
      'field_associated_agencies' => 'footer',
      'field_officer' => 'footer',
      'field_oceanscape_goal' => 'footer',
      'field_notes' => 'footer',
      'field_existing_documents_links' => 'footer',
      'field_project_budget' => 'footer',
      'field_sdg_goal' => 'footer',
      'field_mdg_goal' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|project|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|project|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'project';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_project_status',
        2 => 'body',
        3 => 'group_project_dates',
        15 => 'field_date',
      ),
      'footer' => array(
        4 => 'group_location',
        14 => 'field_sdg_goal',
        16 => 'field_mdg_goal',
        18 => 'field_oceanscape_goal',
        19 => 'group_goals',
        27 => 'field_locations',
      ),
      'right' => array(
        5 => 'field_associated_agencies',
        6 => 'group_budget',
        7 => 'group_researchers',
        8 => 'group_agencies',
        9 => 'field_existing_documents_links',
        10 => 'group_documents_links',
        11 => 'field_project_budget',
        12 => 'group_keyword',
        13 => 'group_notes',
        17 => 'group_access_control',
        20 => 'field_notes',
        23 => 'field_access_control',
        24 => 'field_officer',
        25 => 'field_researcher',
      ),
      'hidden' => array(
        21 => 'path',
        22 => 'field_epog_id',
        26 => 'field_funding_agency_id',
        28 => 'field_somethingorother_goals',
        29 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_project_status' => 'left',
      'body' => 'left',
      'group_project_dates' => 'left',
      'group_location' => 'footer',
      'field_associated_agencies' => 'right',
      'group_budget' => 'right',
      'group_researchers' => 'right',
      'group_agencies' => 'right',
      'field_existing_documents_links' => 'right',
      'group_documents_links' => 'right',
      'field_project_budget' => 'right',
      'group_keyword' => 'right',
      'group_notes' => 'right',
      'field_sdg_goal' => 'footer',
      'field_date' => 'left',
      'field_mdg_goal' => 'footer',
      'group_access_control' => 'right',
      'field_oceanscape_goal' => 'footer',
      'group_goals' => 'footer',
      'field_notes' => 'right',
      'path' => 'hidden',
      'field_epog_id' => 'hidden',
      'field_access_control' => 'right',
      'field_officer' => 'right',
      'field_researcher' => 'right',
      'field_funding_agency_id' => 'hidden',
      'field_locations' => 'footer',
      'field_somethingorother_goals' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 0,
  );
  $export['node|project|form'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function mpub_projectdb_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'countries';
  $ds_view_mode->label = 'Countries';
  $ds_view_mode->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $export['countries'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'regions';
  $ds_view_mode->label = 'Regions';
  $ds_view_mode->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $export['regions'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'titles_only';
  $ds_view_mode->label = 'Titles Only';
  $ds_view_mode->entities = array(
    'field_collection_item' => 'field_collection_item',
  );
  $export['titles_only'] = $ds_view_mode;

  return $export;
}
