<?php
/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * BoxSelect Behavior
 */
class openlayers_behavior_boxselections extends openlayers_behavior {
  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
      'input_fields' => '',
    );
  }

  function js_dependency() {
    return array(
      'OpenLayers.Control.DrawFeature',
      'OpenLayers.Layer.Vector',
      'OpenLayers.Handler.RegularPolygon'
    );
  }

  function options_form($defaults = array()) {
    return array(
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
    drupal_add_js(drupal_get_path('module', 'mpub_projectdb') .
      '/openlayers/behavior_boxselections.js');
    return $this->options;
  }
}
