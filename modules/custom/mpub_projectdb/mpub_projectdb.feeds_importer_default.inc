<?php
/**
 * @file
 * mpub_projectdb.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function mpub_projectdb_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'projects_import';
  $feeds_importer->config = array(
    'name' => 'Projects Import',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 0,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'EPOG_ID',
            'target' => 'field_epog_id',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Funding agency ID/Project Code/link',
            'target' => 'field_funding_agency_id',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Project name/title*',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Start date*',
            'target' => 'field_date:start',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'End date*',
            'target' => 'field_date:end',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Project summary/abstract*',
            'target' => 'body',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'project',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['projects_import'] = $feeds_importer;

  return $export;
}
