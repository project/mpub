<?php
/**
 * @file
 * mpub_map_area.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mpub_map_area_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create value_area_of_interest content'.
  $permissions['create value_area_of_interest content'] = array(
    'name' => 'create value_area_of_interest content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any value_area_of_interest content'.
  $permissions['delete any value_area_of_interest content'] = array(
    'name' => 'delete any value_area_of_interest content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own value_area_of_interest content'.
  $permissions['delete own value_area_of_interest content'] = array(
    'name' => 'delete own value_area_of_interest content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any value_area_of_interest content'.
  $permissions['edit any value_area_of_interest content'] = array(
    'name' => 'edit any value_area_of_interest content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own value_area_of_interest content'.
  $permissions['edit own value_area_of_interest content'] = array(
    'name' => 'edit own value_area_of_interest content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
