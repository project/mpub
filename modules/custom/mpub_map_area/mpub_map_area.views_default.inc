<?php
/**
 * @file
 * mpub_map_area.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mpub_map_area_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'value_area_related_maps';
  $view->description = 'List of links to all published map pages';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Value Area Related Maps';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Related Maps';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Find Maps';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'responsive_grid';
  $handler->display->display_options['style_options']['columns'] = '3';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_field_related_maps_node']['id'] = 'reverse_field_related_maps_node';
  $handler->display->display_options['relationships']['reverse_field_related_maps_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_related_maps_node']['field'] = 'reverse_field_related_maps_node';
  $handler->display->display_options['relationships']['reverse_field_related_maps_node']['required'] = TRUE;
  /* Field: Content: Preview Image */
  $handler->display->display_options['fields']['field_preview_image']['id'] = 'field_preview_image';
  $handler->display->display_options['fields']['field_preview_image']['table'] = 'field_data_field_preview_image';
  $handler->display->display_options['fields']['field_preview_image']['field'] = 'field_preview_image';
  $handler->display->display_options['fields']['field_preview_image']['label'] = '';
  $handler->display->display_options['fields']['field_preview_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_preview_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_preview_image']['settings'] = array(
    'image_style' => 'medium',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_preview_image']['group_column'] = 'entity_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_map_description']['id'] = 'field_map_description';
  $handler->display->display_options['fields']['field_map_description']['table'] = 'field_data_field_map_description';
  $handler->display->display_options['fields']['field_map_description']['field'] = 'field_map_description';
  $handler->display->display_options['fields']['field_map_description']['label'] = '';
  $handler->display->display_options['fields']['field_map_description']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_map_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_map_description']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['field_map_description']['settings'] = array(
    'trim_length' => '600',
  );
  /* Field: Content: Map Category */
  $handler->display->display_options['fields']['field_map_category']['id'] = 'field_map_category';
  $handler->display->display_options['fields']['field_map_category']['table'] = 'field_data_field_map_category';
  $handler->display->display_options['fields']['field_map_category']['field'] = 'field_map_category';
  $handler->display->display_options['fields']['field_map_category']['label'] = '';
  $handler->display->display_options['fields']['field_map_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_map_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_map_category']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Layers */
  $handler->display->display_options['fields']['field_layers']['id'] = 'field_layers';
  $handler->display->display_options['fields']['field_layers']['table'] = 'field_data_field_layers';
  $handler->display->display_options['fields']['field_layers']['field'] = 'field_layers';
  $handler->display->display_options['fields']['field_layers']['label'] = '';
  $handler->display->display_options['fields']['field_layers']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_layers']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_layers']['alter']['text'] = '<i class="fa fa-database"></i>[field_layers]';
  $handler->display->display_options['fields']['field_layers']['element_label_colon'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['exclude'] = TRUE;
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'd-M-Y';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['text'] = 'View Map';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="views-field-field-layers">[field_layers]</div>
<div class="views-field-created">[created]</div>
<div class="views-field-view-node">[view_node]</div>';
  $handler->display->display_options['fields']['nothing']['element_class'] = 'map-item-links';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'reverse_field_related_maps_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Related Maps (field_related_maps) */
  $handler->display->display_options['arguments']['field_related_maps_target_id']['id'] = 'field_related_maps_target_id';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['table'] = 'field_data_field_related_maps';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['field'] = 'field_related_maps_target_id';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['relationship'] = 'reverse_field_related_maps_node';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_related_maps_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'map_page' => 'map_page',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Global: Combine fields filter */
  $handler->display->display_options['filters']['combine']['id'] = 'combine';
  $handler->display->display_options['filters']['combine']['table'] = 'views';
  $handler->display->display_options['filters']['combine']['field'] = 'combine';
  $handler->display->display_options['filters']['combine']['operator'] = 'word';
  $handler->display->display_options['filters']['combine']['group'] = 1;
  $handler->display->display_options['filters']['combine']['exposed'] = TRUE;
  $handler->display->display_options['filters']['combine']['expose']['operator_id'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['label'] = 'Search for Maps';
  $handler->display->display_options['filters']['combine']['expose']['operator'] = 'combine_op';
  $handler->display->display_options['filters']['combine']['expose']['identifier'] = 'combine';
  $handler->display->display_options['filters']['combine']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['combine']['fields'] = array(
    'title' => 'title',
    'field_map_description' => 'field_map_description',
    'field_map_category' => 'field_map_category',
  );
  /* Filter criterion: Content: Map Category (field_map_category) */
  $handler->display->display_options['filters']['field_map_category_tid']['id'] = 'field_map_category_tid';
  $handler->display->display_options['filters']['field_map_category_tid']['table'] = 'field_data_field_map_category';
  $handler->display->display_options['filters']['field_map_category_tid']['field'] = 'field_map_category_tid';
  $handler->display->display_options['filters']['field_map_category_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_map_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_map_category_tid']['expose']['operator_id'] = 'field_map_category_tid_op';
  $handler->display->display_options['filters']['field_map_category_tid']['expose']['label'] = 'Filter';
  $handler->display->display_options['filters']['field_map_category_tid']['expose']['operator'] = 'field_map_category_tid_op';
  $handler->display->display_options['filters']['field_map_category_tid']['expose']['identifier'] = 'field_map_category_tid';
  $handler->display->display_options['filters']['field_map_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_map_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_map_category_tid']['vocabulary'] = 'map_categories';

  /* Display: Related Maps Block */
  $handler = $view->new_display('block', 'Related Maps Block', 'block_1');
  $handler->display->display_options['block_description'] = 'area_related_maps_block';
  $export['value_area_related_maps'] = $view;

  return $export;
}
