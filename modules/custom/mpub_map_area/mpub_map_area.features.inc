<?php
/**
 * @file
 * mpub_map_area.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpub_map_area_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mpub_map_area_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function mpub_map_area_node_info() {
  $items = array(
    'value_area_of_interest' => array(
      'name' => t('Value / Area of Interest'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
