<?php
/**
 * @file
 * mpub_map_area.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function mpub_map_area_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'value-area_of_interest_page';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'value_area_of_interest' => 'value_area_of_interest',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-value_area_related_maps-block_1' => array(
          'module' => 'views',
          'delta' => 'value_area_related_maps-block_1',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['value-area_of_interest_page'] = $context;

  return $export;
}
