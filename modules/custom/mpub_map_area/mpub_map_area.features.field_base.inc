<?php
/**
 * @file
 * mpub_map_area.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function mpub_map_area_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_related_maps'.
  $field_bases['field_related_maps'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_related_maps',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'map_page' => 'map_page',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_simple_layers'.
  $field_bases['field_simple_layers'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_simple_layers',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => '_mpub_layer_available_layers',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
