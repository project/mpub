<?php
/**
 * @file
 * mpub_site_config.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function mpub_site_config_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'footer_blocks';
  $mini->category = '';
  $mini->admin_title = 'Footer Blocks';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '472b0c22-fd83-4eec-a2b6-7ea39c007f03';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-e46448bf-ea39-4aef-99a3-9dff73ae475a';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'maps_overview-recent_maps_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e46448bf-ea39-4aef-99a3-9dff73ae475a';
    $display->content['new-e46448bf-ea39-4aef-99a3-9dff73ae475a'] = $pane;
    $display->panels['middle'][0] = 'new-e46448bf-ea39-4aef-99a3-9dff73ae475a';
    $pane = new stdClass();
    $pane->pid = 'new-33b6848c-b066-49e1-8361-fd67e9035ae5';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-quick-links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '33b6848c-b066-49e1-8361-fd67e9035ae5';
    $display->content['new-33b6848c-b066-49e1-8361-fd67e9035ae5'] = $pane;
    $display->panels['right'][0] = 'new-33b6848c-b066-49e1-8361-fd67e9035ae5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-e46448bf-ea39-4aef-99a3-9dff73ae475a';
  $mini->display = $display;
  $export['footer_blocks'] = $mini;

  return $export;
}
