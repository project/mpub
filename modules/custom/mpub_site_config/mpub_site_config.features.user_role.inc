<?php
/**
 * @file
 * mpub_site_config.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mpub_site_config_user_default_roles() {
  $roles = array();

  // Exported role: content editor.
  $roles['content editor'] = array(
    'name' => 'content editor',
    'weight' => 3,
  );

  return $roles;
}
