<?php
/**
 * @file
 * mpub_map_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function mpub_map_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'home';
  $page->task = 'page';
  $page->admin_title = 'Home';
  $page->admin_description = '';
  $page->path = 'home';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_home__panel_context_6e29e81e-42f5-4645-8bde-e19677b3c818';
  $handler->task = 'page';
  $handler->subtask = 'home';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'mpub-home';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'branding' => NULL,
      'header' => NULL,
      'navigation' => NULL,
      'content' => NULL,
      'footer' => NULL,
      'first' => NULL,
      'second' => NULL,
      'third' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'TEST';
  $display->uuid = '8851aa7c-6d28-48f3-9cbe-bc380b206f25';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5e039d82-28c1-4a8f-92ad-38c87541d945';
    $pane->panel = 'maps_overview';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'About Maps',
      'title' => 'About Maps',
      'body' => '<p>[PLACEHOLDER CONTENT]<br/><a href="/maps" title="View Maps">View Maps</a></p>',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5e039d82-28c1-4a8f-92ad-38c87541d945';
    $display->content['new-5e039d82-28c1-4a8f-92ad-38c87541d945'] = $pane;
    $display->panels['maps_overview'][0] = 'new-5e039d82-28c1-4a8f-92ad-38c87541d945';
    $pane = new stdClass();
    $pane->pid = 'new-28d8477f-016c-44a3-b047-2adf2fc836ea';
    $pane->panel = 'maps_overview';
    $pane->type = 'views_panes';
    $pane->subtype = 'maps_overview-featured_maps_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '28d8477f-016c-44a3-b047-2adf2fc836ea';
    $display->content['new-28d8477f-016c-44a3-b047-2adf2fc836ea'] = $pane;
    $display->panels['maps_overview'][1] = 'new-28d8477f-016c-44a3-b047-2adf2fc836ea';
    $pane = new stdClass();
    $pane->pid = 'new-42fbe705-eb38-435b-8a0b-1885a9597a9c';
    $pane->panel = 'maps_overview';
    $pane->type = 'views_panes';
    $pane->subtype = 'maps_overview-recent_maps_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '42fbe705-eb38-435b-8a0b-1885a9597a9c';
    $display->content['new-42fbe705-eb38-435b-8a0b-1885a9597a9c'] = $pane;
    $display->panels['maps_overview'][2] = 'new-42fbe705-eb38-435b-8a0b-1885a9597a9c';
    $pane = new stdClass();
    $pane->pid = 'new-fb0b9b72-9fb3-4e6d-a61f-e76e9fbd69cc';
    $pane->panel = 'projects_overview';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Projects Overview',
      'title' => 'Projects Overview',
      'body' => '<p>[PLACEHOLDER CONTENT]<br/><a href="/projects" title="View Projects">View Projects</a></p>',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fb0b9b72-9fb3-4e6d-a61f-e76e9fbd69cc';
    $display->content['new-fb0b9b72-9fb3-4e6d-a61f-e76e9fbd69cc'] = $pane;
    $display->panels['projects_overview'][0] = 'new-fb0b9b72-9fb3-4e6d-a61f-e76e9fbd69cc';
    $pane = new stdClass();
    $pane->pid = 'new-d5cf12d6-aa86-4f33-9ae3-7916679f111c';
    $pane->panel = 'projects_overview';
    $pane->type = 'views_panes';
    $pane->subtype = 'projects-featured_projects_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'd5cf12d6-aa86-4f33-9ae3-7916679f111c';
    $display->content['new-d5cf12d6-aa86-4f33-9ae3-7916679f111c'] = $pane;
    $display->panels['projects_overview'][1] = 'new-d5cf12d6-aa86-4f33-9ae3-7916679f111c';
    $pane = new stdClass();
    $pane->pid = 'new-cddc6d23-f5b1-4729-ac91-b670536abe72';
    $pane->panel = 'projects_overview';
    $pane->type = 'views_panes';
    $pane->subtype = 'projects-recent_projects_pane';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'cddc6d23-f5b1-4729-ac91-b670536abe72';
    $display->content['new-cddc6d23-f5b1-4729-ac91-b670536abe72'] = $pane;
    $display->panels['projects_overview'][2] = 'new-cddc6d23-f5b1-4729-ac91-b670536abe72';
    $pane = new stdClass();
    $pane->pid = 'new-6ed3311a-c6b8-470b-a630-edf8d4245b05';
    $pane->panel = 'top';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Getting Started Text',
      'title' => 'Getting Started',
      'body' => '<p>You are viewing the EPOG map publisher profile for Drupal.</p><h3>Site Configuration Tasks</h3><ol><li><a href="/user">Login as the admin superuser</a></li><li><a href="/admin/appearance/settings/mpubtheme">Upload organisation logo</a></li><li><a href="/admin/appearance/flexicolor">Select site color profile</a></li><li><a href="/admin/people/create">Create a content editor user</a></li><li><a href="/admin/structure/taxonomy/map_categories">Create some category&nbsp;terms to group map pages by.</a></li><li><a href="/admin/structure/taxonomy/department">Create some project agencies</a></li><li>Change this introductory text (select &#39;Customize this page&#39; and then edit the pane&#39;).</li><li><a href="/admin/structure/openlayers/layers/add%3Flayer_type%3Dmpub_map_page_wms">Create some layers</a></li><li><a href="/node/add/map-page">Create some map pages!</a></li><li><a href="/node/add/project">Create some projects!</a></li></ol><p>&nbsp;</p>',
      'format' => 'filtered_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array();
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '6ed3311a-c6b8-470b-a630-edf8d4245b05';
    $display->content['new-6ed3311a-c6b8-470b-a630-edf8d4245b05'] = $pane;
    $display->panels['top'][1] = 'new-6ed3311a-c6b8-470b-a630-edf8d4245b05';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['home'] = $page;

  return $pages;

}
