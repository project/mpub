<?php
/**
 * @file
 * mpub_test_content.openlayers_layers.inc
 */

/**
 * Implements hook_openlayers_layers().
 */
function mpub_test_content_openlayers_layers() {
  $export = array();

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'change_in_sea_surface_temperature';
  $openlayers_layers->title = 'Change in Sea Surface temperature';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/nerp/wms?version=1.1.0',
    'params' => array(
      'buffer' => '2',
      'ratio' => '1.5',
      'singleTile' => FALSE,
      'metadata' => 'http://www.cmar.csiro.au/geonetwork/srv/en/iso19139.xml?id=51805',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => 'ALTT_31July2014',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['change_in_sea_surface_temperature'] = $openlayers_layers;

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'key_ecological_features';
  $openlayers_layers->title = 'Key Ecological Features';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/local/wms?version=1.1.0',
    'params' => array(
      'buffer' => '2',
      'ratio' => '1.5',
      'singleTile' => 0,
      'metadata' => 'http://catalogue.aodn.org.au/geonetwork/srv/eng/xml_iso19139.mcp-1.4?id=471291&styleSheet=xml_iso19139.mcp-1.4.xsl',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => 'KeyEcologicalFeatures',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['key_ecological_features'] = $openlayers_layers;

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'oil_and_gas_facilities';
  $openlayers_layers->title = 'Oil and Gas Facilities';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/nerp/wms?version=1.1.0',
    'params' => array(
      'buffer' => 2,
      'ratio' => 1.5,
      'singleTile' => FALSE,
      'metadata' => 'http://www.cmar.csiro.au/geonetwork/srv/en/iso19139.xml?id=51764',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => 'Data_All_Petroleum_Facilities_2013_GA-marine',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['oil_and_gas_facilities'] = $openlayers_layers;

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'seismic_surveys_1996_2000_2d';
  $openlayers_layers->title = 'Seismic Surveys - 1996- 2000 2D';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/nerp/wms?version=1.1.0',
    'params' => array(
      'buffer' => 2,
      'ratio' => 1.5,
      'singleTile' => FALSE,
      'metadata' => 'http://www.cmar.csiro.au/geonetwork/srv/en/iso19139.xml?id=51756',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => '1996-2000-Data_All_SeismicSurveys__2D_2013_09',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['seismic_surveys_1996_2000_2d'] = $openlayers_layers;

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'seismic_surveys_pre_1996_2d';
  $openlayers_layers->title = 'Seismic Surveys - Pre 1996 2D';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/nerp/wms?version=1.1.0',
    'params' => array(
      'buffer' => 2,
      'ratio' => 1.5,
      'singleTile' => FALSE,
      'metadata' => 'http://www.cmar.csiro.au/geonetwork/srv/en/iso19139.xml?id=51756',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => 'Pre-1996-Data_All_SeismicSurveys__2D_2013_09',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['seismic_surveys_pre_1996_2d'] = $openlayers_layers;

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'shipping_2010';
  $openlayers_layers->title = 'Shipping 2010';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/nerp/wms?version=1.1.0',
    'params' => array(
      'buffer' => 2,
      'ratio' => 1.5,
      'singleTile' => FALSE,
      'metadata' => 'http://www.cmar.csiro.au/geonetwork/srv/en/iso19139.xml?id=47238',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => 'AMSA-Shipping-2010',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['shipping_2010'] = $openlayers_layers;

  $openlayers_layers = new stdClass();
  $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
  $openlayers_layers->api_version = 1;
  $openlayers_layers->name = 'shipping_2011';
  $openlayers_layers->title = 'Shipping 2011';
  $openlayers_layers->description = '';
  $openlayers_layers->data = array(
    'base_url' => 'https://www.cmar.csiro.au/geoserver/nerp/wms?version=1.1.0',
    'params' => array(
      'buffer' => 2,
      'ratio' => 1.5,
      'singleTile' => FALSE,
      'metadata' => 'http://www.cmar.csiro.au/geonetwork/srv/en/iso19139.xml?id=47238',
    ),
    'options' => array(
      'TRANSPARENT' => 'true',
      'exceptions' => 'application/vnd.ogc.se_inimage',
      'format' => 'image/png',
      'layers' => array(
        0 => 'AMSA-Shipping-2011',
      ),
      'styles' => '',
    ),
    'projection' => array(
      0 => 'EPSG:900913',
    ),
    'isBaseLayer' => FALSE,
    'resolutions' => array(),
    'layer_type' => 'mpub_map_page_wms',
    'get_layer_ids' => 'Get Layer Info',
    'layer_handler' => 'wms',
    'transitionEffect' => 'resize',
    'weight' => 0,
  );
  $export['shipping_2011'] = $openlayers_layers;

  return $export;
}
