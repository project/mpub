<?php
/**
 * @file
 * mpub_map_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpub_map_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mpub_map_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function mpub_map_page_node_info() {
  $items = array(
    'map_page' => array(
      'name' => t('Map Page'),
      'base' => 'node_content',
      'description' => t('Displays an OpenLayers map on a page using a custom URL.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
