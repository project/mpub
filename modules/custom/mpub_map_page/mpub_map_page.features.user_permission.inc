<?php
/**
 * @file
 * mpub_map_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function mpub_map_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access administration pages'.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'system',
  );

  // Exported permission: 'add terms in map_categories'.
  $permissions['add terms in map_categories'] = array(
    'name' => 'add terms in map_categories',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'administer taxonomy'.
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'create map_page content'.
  $permissions['create map_page content'] = array(
    'name' => 'create map_page content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any map_page content'.
  $permissions['delete any map_page content'] = array(
    'name' => 'delete any map_page content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own map_page content'.
  $permissions['delete own map_page content'] = array(
    'name' => 'delete own map_page content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in map_categories'.
  $permissions['delete terms in map_categories'] = array(
    'name' => 'delete terms in map_categories',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any map_page content'.
  $permissions['edit any map_page content'] = array(
    'name' => 'edit any map_page content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own map_page content'.
  $permissions['edit own map_page content'] = array(
    'name' => 'edit own map_page content',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in map_categories'.
  $permissions['edit terms in map_categories'] = array(
    'name' => 'edit terms in map_categories',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'view the administration theme'.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      'content editor' => 'content editor',
    ),
    'module' => 'system',
  );

  return $permissions;
}
