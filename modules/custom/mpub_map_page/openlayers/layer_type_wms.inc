<?php
/**
 * @file
 * Implementation of OpenLayers layer type.
 */

/**
 * OpenLayers layer type plugin 'mpub_map_page_wms'.
 *
 * Extends the bundled WMS layer type to include additional fields and
 * hide some irrelevant fields.
 */
class mpub_map_page_layer_type_wms extends openlayers_layer_type_wms {

  /**
   * Overrides openlayers_layer_type_wms::options_init().
   */
  function options_init() {
    $options = parent::options_init();

    //Set up the defaults
    $options['isBaseLayer'] = FALSE;
    $options['options']['TRANSPARENT'] = TRUE;
    $options['options']['format'] = 'image/png';
    $options['options']['exceptions'] = 'application/vnd.ogc.se_inimage';
    $options['options']['layers'] = array();
    $options['base_url'] = variable_get('mpub_default_geoserver','');


    return $options;
  }

  /**
   * Overrides openlayers_layer_type_wms::options_form().
   *
   * Hides many of the options from the WMS layer type and adds new options
   * for metadata and layer group.
   */
  function options_form($defaults = array()) {
    $form = parent::options_form($defaults);

    // Add in options from the base class. Openlayers does some silly merging
    // for options forms instead of relying on classes to merge their parents.
    // in openlayers_layers_ui::edit_form().
    $form += openlayers_layer_type::options_form($this);

    $form['isBaseLayer']['#access'] = FALSE;

    $form['base_url']['#description'] = t('default: !default', array('!default' => variable_get('mpub_default_geoserver','')));

    $params = &$form['params'];

    $params['buffer']['#access'] = FALSE;
    $params['ratio']['#access'] = FALSE;
    $params['singleTile']['#access'] = FALSE;
    $params['singleTile']['#default_value'] = FALSE;

    $params['metadata'] = array(
      '#type' => 'textfield',
      '#title' => t('Metadata URL override'),
      '#description' => t('Enter the URL for ISO19115/19139 XML metadata related to this layer'),
      '#default_value' => isset($this->data['params']['metadata']) ? $this->data['params']['metadata'] : '',
      '#maxlength' => 500,
    );

    // collect a group
    $params['layer_group'] = array(
      '#type' => 'textfield',
      '#title' => t('Layer Group'),
      '#description' => t('Used to group layers in administrative lists only.'),
      '#default_value' => isset($this->data['params']['layer_group']) ? $this->data['params']['layer_group'] : '',
      '#size' => 60,
      '#maxlength' => 128,
    );

    if(isset($form['options'])) {
      $options = &$form['options'];
      $options['TRANSPARENT']['#access'] = FALSE;
      $options['exceptions']['#access'] = FALSE;
      $options['styles']['#access'] = FALSE;
      $options['format']['#access'] = FALSE;

      //This is turned in to an autocomplete in mpub_map_page_form_alter
      //Because we need $form_state for the ajax callback, we can't do it here
//      $options['layers']['#type'] = 'textfield';
//      $options['layers']['#size'] = 120;
//      $options['layers']['#description'] = t('Enter the layer ID(s) for this layer, e.g. nerp:AMSA-Shipping-2010 or AMSA-Shipping-2010. Comma separated.');
//      drupal_set_message('<pre>'.print_r($this->data['options'],true).'</pre>');
//      $options['layers']['#default_value'] = !empty($this->data['options']) ? reset($this->data['options']['layers']) : null;
    }

    $form['projection']['#access'] = FALSE;

    // Suppress warnings from openlayers_layer_type::options_form_submit().
    $form['resolutions'] = array(
      '#type' => 'value',
      '#value' => array(),
    );


    return $form;
  }


  /**
   * Overrides openlayers_layer_type_wms::options_validate().
   *
   * Explodes the layers by comma because we're using tag style autocomplete
   */
  function options_form_validate($form, &$form_state) {

    $layers = array_map('trim', explode(',', $form_state['data']['options']['layers']));
    $form_state['data']['options']['layers'] = $layers;

    //use the projection info from the layer info
    foreach($form_state['data']['options']['layers'] as $layer_name) {
      $layer_info = mpub_map_page_get_layer_info($form_state);
      if(!empty($layer_info['projection'])) {
        $form_state['data']['projection'] = array();

        foreach($layer_info['projection'] as $proj) {
          if(in_array($proj, $form['mpub_map_page_wms']['projection']['#options'])) {
            $form_state['data']['projection'][$proj] = $proj;
          }
        }
      }
    }
  }

  /**
   * Overrides openlayers_layer_type_wms::render().
   */
  function render(&$map) {
  	$identifiers = array(
    	$map['projection'],
    	$map['displayProjection'],
  	) + $this->data['projection'];

    $projections = ctools_export_load_object('openlayers_projections', 'conditions', array('identifier' => $identifiers));
    foreach($projections as $identifier => $projection) {
      proj4js_load_definition($identifier, $projection->definition);
    }

    drupal_add_js(drupal_get_path('module', 'openlayers') . '/plugins/layer_types/openlayers_layer_type_wms.js');
    drupal_add_js(drupal_get_path('module', 'mpub_map_page') . '/openlayers/layer_type_wms.js');
  }
}
