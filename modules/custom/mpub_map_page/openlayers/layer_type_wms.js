/**
 * @file
 * Layer handler for NERP Marine WMS layers.
 */

// Use the default WMS layer handler.
Drupal.openlayers.layer.mpub_map_page_wms = Drupal.openlayers.layer.wms;
