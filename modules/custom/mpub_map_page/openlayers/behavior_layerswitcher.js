/**
 * @file
 * Behavior implementation for NERP Layer Switcher.
 */

/**
 * Implementation for mpub_map_page_layerswitcher behavior.
 *
 * Adds interaction to layer form rendered by the server.
 */
Drupal.openlayers.addBehavior('mpub_map_page_layerswitcher', function (data, options) {

  var mapContainer = jQuery('#' + data.map.id);
  var metadataContainer = jQuery('#' + options.metadata_container);
  var controlsContainer = jQuery('#' + options.container);

  // Add the entire rendered form.
  controlsContainer.append(options.form);

  jQuery('input.layer:checked').each(function(k,v){
    updateLayerVisibility(jQuery(this).attr('data-layer-name'));
  });

  updatePrintLink();

  //Tree expand/collapse logic
  jQuery('.expcol-link').once('expcol').click(function(event){

    event.preventDefault();
    var $link = jQuery(this);
    var wrapper_class = '.wrapper-'+jQuery(this).attr('id').split('expcol-').pop();

    var collapsing = !$link.hasClass('collapsed');

    jQuery(wrapper_class).slideToggle('fast', function(){
      if(collapsing) {
        $link.addClass('collapsed');
      }
      else {
        $link.removeClass('collapsed');
      }
    });
  });


  // Add loading indicator.
  var indicatorID = data.map.id + '-indicator';
  jQuery('.map-container').append('<div id="' + indicatorID + '" class="map-loading-indicator"></div>');
  var loading = [];
  for (var i=0; i < data.openlayers.layers.length; i++) {
    var layer = data.openlayers.layers[i];

    if (typeof layer.events != 'undefined') {

      if(layer.options.isBaseLayer) {
        layer.events.register('loadend', layer, function() {
          jQuery('input.layer:checked').each(function(k,v){
            updateLayerVisibility(jQuery(this).attr('data-layer-name'));
          });
        });
      }

      layer.events.register('loadstart', layer, function() {
        loading.push(layer.id);
        updateIndicator();
      });
      layer.events.register('loadend', layer, function() {
        var index = loading.indexOf(layer.id);
        if (index > -1) {
          loading.splice(index, 1);
        }
        updateIndicator();
      });
    };
  }

  // Updates the loading indicator.
  function updateIndicator() {
    if (loading.length > 0) {
      jQuery('#' + indicatorID).html('<i class="fa fa-spinner fa-spin"></i>').addClass('loading');
    }
    else {
     jQuery('#' + indicatorID).html('').removeClass('loading');
    }
  }

  // Shows/hides layers.
  function updateLayerVisibility(id) {
    var layers = data.openlayers.layers.slice();

    if(jQuery('.layer[data-layer-name="'+id+'"]:checked').length == 0) {
      visible = 0;
    }
    else {
      visible = 1;
    }

    for (var i = 0; i < layers.length; i++) {
      if (layers[i].drupalID == id) {
        var layer = layers[i];
        layer.setVisibility(visible);
      }
    }
  }

  // Updates the print link to include all active layers.
  function updatePrintLink() {
    var activeLayerNames = controlsContainer.find('input.layer:checked').map(function() {
      return jQuery(this).attr('data-layer-name');
    }).get().join();
    if (Drupal.settings.mpub_map_page.currentNID && activeLayerNames) {
      var printUrl = Drupal.settings.basePath + 'mpub_map_page/metadata/' + Drupal.settings.mpub_map_page.currentNID + '/' + activeLayerNames;
      jQuery('.mpub-print-link').empty().append('<a href="' + printUrl + '"><i class="fa fa-print"></i> Print</a>');
    }
    else {
      jQuery('.mpub-print-link').empty();
    }
  }

  var layerCheckboxes = controlsContainer.find('input.layer');

  layerCheckboxes.click(function() {
    console.log('layer check');
    var checkbox = jQuery(this);
    var layerName = checkbox.attr('data-layer-name');
    updateLayerVisibility(layerName);
    updatePrintLink();
  });


  jQuery('.layers-container label').click(function(e) {

    var layerContainer = jQuery(this).parent();
    var layerName = layerContainer.find('input.layer').attr('data-layer-name');
    var checkbox = layerContainer.find('input.layer[type="checkbox"]');
    var checked = checkbox.is(':checked');

    checkbox.attr('checked', !checked);

    updateLayerVisibility(layerName);
    updatePrintLink();
  });

  var groupCheckboxes = controlsContainer.find('input.group-toggle');

  groupCheckboxes.click(function() {

    var checkbox = jQuery(this);
    var checked = checkbox.is(':checked');
    jQuery.each(checkbox.closest('.group').find('input[type=checkbox]'), function(i, el) {
      var layerName = jQuery(el).attr('data-layer-name');
      var layerCheckbox = jQuery(el);
      layerCheckbox.attr('checked', checked);
      updateLayerVisibility(layerName);
    });
    updatePrintLink();

  });

  jQuery('a.toggle-info').click(function(event){
    event.preventDefault();    
    jQuery('.info-selected').removeClass('info-selected');
    jQuery(this).closest('.form-item').addClass('info-selected');
    var layerName = jQuery(this).attr('data-layer-name');
    showOpacitySlider(layerName);
    showLayerMetadata(layerName);        
  });

  function showOpacitySlider(layerID) {
    var layer = data.openlayers.getLayersBy('drupalID', layerID).pop() || {};
    
    if(layer) {
      jQuery('.layer-controls').show();
      jQuery('.opacity-slider').slider({
        animate: false,
        step: 0.01,
        min: 0,
        max: 1,
        values: [(layer.opacity || 1)],
        slide: function(event, ui){
          layer.setOpacity(ui.value);
        }
      });
    }
  }

  
  function showLayerMetadata(layerName) {

    var url = Drupal.settings.basePath + 'mpub_map_page/ajax/metadata/' + layerName;

    jQuery.ajax({
      url: url,
      dataType: 'json',
      beforeSend: function() {
        metadataContainer.html('<p class="metadata-loading">Loading...</p>');
      },
      error: function(xhr, status, error) {        
        metadataContainer.html('<p class="metadata-error">Information could not be loaded for this layer.</p>');
      },
      success: function(data, status, xhr) {
        jQuery('.map-info-sidebar .layer-title').text(data.title);
        metadataContainer.html(data.html);
        Drupal.attachBehaviors();
      }
    });
  }

});
