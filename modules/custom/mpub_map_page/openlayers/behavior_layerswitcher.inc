<?php
/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * OpenLayers behavior plugin 'mpub_map_page_layerswitcher'.
 *
 * The standard OpenLayers layer switcher cannot be configured to display
 * a hierarchy of layers, and the plugin is not very extensible. Instead of
 * a whole new OpenLayers plugin, we render the form on the server side and
 * add simply add interaction on the client side.
 */
class mpub_map_page_behavior_layerswitcher extends openlayers_behavior {

  private $groups;

  /**
   * Implements openlayers_behavior:options_init().
   */
  function options_init() {
    return array(
      'container' => 'map-sidebar-left',
      'metadata_container' => 'map-sidebar-right',
    );
  }

  /**
   * Implements openlayers_behavior:options_form().
   */
  function options_form($defaults = array()) {
    return array(
      'container' => array(
        '#type' => 'textfield',
        '#title' => t('Container ID'),
        '#description' => t('Enter the ID of the container element for the layer switcher.'),
        '#default_value' => $this->options['container'],
      ),
      'metadata_container' => array(
        '#type' => 'textfield',
        '#title' => t('Metadata Container ID'),
        '#description' => t('Enter the ID of the container element for metadata display.'),
        '#default_value' => $this->options['metadata_container'],
      ),
    );
  }

  /**
   * Implements openlayers_behavior:render().
   */
  function render(&$map) {

    $nid = NULL;
    if($node = menu_get_object('node')) {
      $nid = $node->nid;
      $layers_field_name = 'field_layers'; //TODO This should be configurable on the map field or somewhere else
      $layers = field_get_items('node', $node, $layers_field_name);
      if(!empty($layers)) {
        $layers = reset($layers);
        $layers = $layers['data'];
        $layers = unserialize($layers);
      }
      $map['map_page_layers'] = $layers;
    }

    drupal_add_js(array('mpub_map_page' => array('currentNID' => $nid)), 'setting');
    drupal_add_js(drupal_get_path('module', 'mpub_map_page') . '/openlayers/behavior_layerswitcher.js');

    $options = $this->options;

    $form = $this->layerSwitcherForm($map);
    $options['form'] = render( $form );
    return $options;
  }


  private function categorySelectElement($map, $parent_id = -1) {
    $element = array();

    foreach($this->groups as $group) {

      if($group['pid'] != $parent_id) {
        continue;
      }

      $id = $group['id'];
      $name = $group['name'];
      $depth = $group['depth'];
      $group_id = drupal_html_id('group_'.$id);
      $expanded = $group['expanded'];
      $checked = $group['default'];

      $icons = '<i class="expand fa fa-folder"></i><i class="collapse fa fa-folder-open"></i>';

      $element['category_'.$id] = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array('expcol-depth-'.$depth, 'group'),
        ),
        'group' => array(
          '#type' => 'checkbox',
          '#id' => $group_id,
          '#title' => '<i class="fa fa-square-o unchecked"></i><i class="fa fa-check-square-o checked"></i>'.$name,
          '#checked' => $checked,
          '#attributes' => array(
            'class' => array('group-toggle'),
            'data-layers' => !empty($group['layers'])?array_keys($group['layers']):array(),
          ),
          '#field_prefix' => '<a href="#" id="expcol-'.$group_id.'" class="'.($expanded?'':'collapsed').' expcol-link">'.$icons.'</a>',
        ),
        'layers' => array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('layers-container', 'wrapper-'.$group_id),
            'style' => 'display:'.($expanded?'block':'none')
          ),
          '',
        ),
        'children' => array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('expcol-wrapper', 'wrapper-'.$group_id),
            'style' => 'display:'.($expanded?'block':'none')
          ),
          'data' => $this->categorySelectElement($map, $id)
        ),
      );

      if(!empty($group['layers'])) {
        foreach($group['layers'] as $layer_id => $layer) {
          $info = mpub_map_page_get_layer_info($layer);
          $element['category_'.$id]['layers']['layer_'.$layer_id] = array(
            '#type' => 'checkbox',
            '#suffix' => '<a href="'.url('mpub_map_page/nojs/metadata/'.$layer->name).'" class="toggle-info" data-layer-name="'.$layer->name.'"><i class="fa fa-info"></i></a>
            <a href="javascript:Drupal.behaviors.mpub_layer_zoom.go(\''.$layer->name.'\');"><i class="fa fa-search-plus"></i></a>',
            '#title' => '<i class="fa fa-square-o unchecked"></i><i class="fa fa-check-square-o checked"></i><strong>'.$layer->title.'</strong>',
            '#checked' => $layer->enabled,
            '#attributes' => array(
              'class' => array('layer'),
              'data-layer-id' => $id,
              'data-layer-name' => $layer->name,
              'data-group' => $group_id,
              'data-legend-url' => isset($info['legend_url'])?$info['legend_url']:'',
            ),
          );
        }
      }
    }

    return $element;
  }


  /**
   * Helper to generate a non-functional form to be used on the client side.
   */
  function layerSwitcherForm($map) {

    $form = array(
      '#type' => 'form',
      '#attributes' => array(
        'class' => array('map-layers'),
      ),
    );

    $layers = array();
    if(isset($map['map_page_layers'])) {
      $layers = $map['map_page_layers'];
    }

    // We only include layers when configured to appear in the switcher.
    if (empty($layers)) {
      return $form;
    }

    // Collect all the groups that have been specified by layers.
    $this->groups = array();
    $this->map = $map;

    foreach ($layers as $layer) {
      if($layer['type'] == 'group') {
        $this->groups[ $layer['id'] ] = $layer;
      }
    }
    $layer_js = array();
    foreach ($layers as $layer) {
      if($layer['type'] == 'layer') {
        $name = $layer['name'];
        $group_id = $layer['pid'];
        $openlayers_layer = openlayers_layer_load($layer['name']);
        $openlayers_layer->data['transitionEffect'] = null;
        $openlayers_layer->enabled = $layer['default'];

        $layer_js[$name] = $openlayers_layer->data;
        $layer_js[$name] += mpub_map_page_get_layer_info($openlayers_layer);

        $this->groups[$group_id]['layers'][$layer['id']] = $openlayers_layer;
      }
    }

    drupal_add_js(array('mpub_layers' => $layer_js), 'setting');

    $form += $this->categorySelectElement($map);

    return $form;

  }
}
