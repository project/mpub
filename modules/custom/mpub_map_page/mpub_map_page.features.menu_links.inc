<?php
/**
 * @file
 * mpub_map_page.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function mpub_map_page_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-quick-links_create-a-map-page:node/add/map-page
  $menu_links['menu-quick-links_create-a-map-page:node/add/map-page'] = array(
    'menu_name' => 'menu-quick-links',
    'link_path' => 'node/add/map-page',
    'router_path' => 'node/add/map-page',
    'link_title' => 'Create a Map Page',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-quick-links_create-a-map-page:node/add/map-page',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Create a Map Page');


  return $menu_links;
}
