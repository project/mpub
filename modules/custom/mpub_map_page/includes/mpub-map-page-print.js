(function($) {
  /**
   * Expand/collapse toggling for map layout.
   */
  Drupal.behaviors.nerpmarine_map_print = {
    attach: function(context, settings) {

      //TODO: print after all the loading of map layers is complete
      
      $(document).bind('ready', function(){
        
        $('body').once('initial-zoom', function() {
        
          $.each(Drupal.settings.openlayers.maps, function(k, map){
            
            openlayers_data = $('#'+map.id).data('openlayers');            
            $.each(map.layers, function(layer_name, layer) {
              if(!layer.isBaseLayer) {
                var layer_data = Drupal.settings.mpub_layers[layer_name];
                Drupal.behaviors.mpub_layer_zoom.layer_extents(openlayers_data, layer_data);            
              }
            });
            
          });
        
        });
         
        
      });
      
    }
  };

  
  
})(jQuery);
