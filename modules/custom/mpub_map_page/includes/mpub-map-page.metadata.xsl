<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:php="http://php.net/xsl"
  xmlns:gmd="http://www.isotc211.org/2005/gmd"
  xmlns:gco="http://www.isotc211.org/2005/gco"
  exclude-result-prefixes="xsl php gmd gco">

  <xsl:output omit-xml-declaration="yes"/>

  <xsl:template match="/">
    <div class="map-metadata">
      <h3 class="label">Abstract:</h3>
      <p class="item"><xsl:value-of select="//gmd:abstract/gco:CharacterString"/></p>

      <h3 class="label">Contact:</h3>
      <p class="item">
        <div><xsl:value-of select="//gmd:CI_ResponsibleParty/gmd:individualName/gco:CharacterString"/></div>
        <div><xsl:value-of select="//gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString"/></div>
        <div><xsl:value-of select="//gmd:CI_ResponsibleParty/gmd:contactInfo//gmd:electronicMailAddress/gco:CharacterString"/></div>
      </p>

      <xsl:if test="$source_url != ''">
        <h3 class="label">Source URL:</h3>
        <p class="item">
          <a>
            <xsl:attribute name="href"><xsl:value-of select="$source_url"/></xsl:attribute>
            <xsl:value-of select="$source_url"/>
          </a>
        </p>
      </xsl:if>


      <xsl:if test="$download_list != ''">
        <div class="download-options">
          <h3 class="label">Download:</h3>
          <p class="item"><label>Select Format</label>
            <xsl:value-of select="$download_list" disable-output-escaping="yes"/>
          </p>
        </div>
      </xsl:if>

    </div>
  </xsl:template>

</xsl:stylesheet>
