<table class="map-layout collapsed-right" cellpadding="0" cellspacing="0">
  <tr>
    <td class="map-sidebar map-layers-sidebar">
      <div class="map-sidebar-inner">
        <h2 class="map-sidebar-heading">Layer Control</h2>
        <div id="map-sidebar-left" class="map-sidebar-left map-sidebar-content">
        </div>
      </div>
    </td>

    <td class="toggle toggle-left"><i class="fa fa-chevron-left"></i></td>

    <td id="map-container" class="map-container">
      <?php print $content['map']; ?>
    </td>


    <td class="toggle toggle-right"><i class="fa fa-chevron-right"></i></td>

    <td class="map-sidebar map-info-sidebar">
        <div class="map-sidebar-inner">
          <a class="layer-info-modal-close toggle-right" href="#"><i class="fa fa-close"></i></a>
          <h2 class="map-sidebar-heading"><i class="fa fa-info-circle"></i>&nbsp;<span class="layer-title">Metadata</span><br/><span class="mpub-print-link"></span></h2>
          
          <div class="layer-controls">
            <div class="control-label">Layer Opacity</div>
            <div class="opacity-slider"></div>
          </div>
          
          <div id="map-sidebar-right" class="map-sidebar-right map-sidebar-content">
            <div class="metadata-message">Select a layer to see information about that layer.</div>
          </div>
        </div>
    </td>
  </tr>
</table>
