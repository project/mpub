<?php
/**
 * @file
 * mpub_export_import.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mpub_export_import_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mpub_export_import_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
